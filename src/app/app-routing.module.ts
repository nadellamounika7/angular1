import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ApiComponent } from './api/api.component';
import { CanDeactivateGuardService } from './can-deactivate-guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DatashareService } from './datashare.service';
import { FeaturesComponent } from './features/features.component';
import { FormsComponent } from './forms/forms.component';
import { HomeComponent } from './home/home.component';
import { Homechild1Component } from './home/homechild1/homechild1.component';
import { Homechild2Component } from './home/homechild1/homechild2/homechild2.component';
import { InputComponent } from './input/input.component';
import { LoginComponent } from './login/login.component';
import { ObservebleComponent } from './observeble/observeble.component';
import { ChildComponent } from './parent1/child/child.component';
import { Parent1Component } from './parent1/parent1.component';
import { StructuralComponent } from './structural/structural.component';
import { UpdatesComponent } from './updates/updates.component';

const routes: Routes = [
  {path:'',redirectTo:'home',pathMatch:'full'},

  {path:'home',component:HomeComponent, children:
[{path:'homechild1',component:Homechild1Component,children:
[{path:'homechild2',component:Homechild2Component}]}]},
  {path:'dashboard',component:DashboardComponent},
  {path:'about',component:AboutComponent},
  {path:'features',component:FeaturesComponent},
  {path:'updates',component:UpdatesComponent},
  {path:'structural',component:StructuralComponent},
  {path:'input',component:InputComponent},
  {path:'parent1',component:Parent1Component,children:
  [{path:'child',component:ChildComponent}]},
  
  {path:'api',component:ApiComponent,canDeactivate:[CanDeactivateGuardService]},
  {path:'observeble', component:ObservebleComponent},
  {
    path: 'lazy1',
    loadChildren: () => import('./lazy1/lazy1.module').then(m => m.Lazy1Module)
  },
  {path:'login',component:LoginComponent},
  {path:'forms',component:FormsComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
