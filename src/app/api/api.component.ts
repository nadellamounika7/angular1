import { Component, OnInit } from '@angular/core';
import { Observable} from 'rxjs/internal/Observable';


@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {

  // constructor() { }

  ngOnInit(): void {
  }
  name:any;
  receiveData(data:any){
    this.name=data.name;
  }

  unSaved: boolean = true;        
    constructor() { }

    canDeactivate(): Observable<boolean> | boolean {


        if (this.unSaved) {

          let result = window.confirm('There are unsaved changes! Are you sure?');

           return result;
        }
        return true;
    }   
  
}
