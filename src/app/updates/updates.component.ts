import { Component, OnInit } from '@angular/core';
import { ObserService } from '../obser.service';

@Component({
  selector: 'app-updates',
  templateUrl: './updates.component.html',
  styleUrls: ['./updates.component.css']
})
export class UpdatesComponent implements OnInit {

  constructor(private ObserService:ObserService) { 

  }
  sendMessage(): void{
    this.ObserService.sendMessage('message from observeble to update');
  }

  ngOnInit(): void {
  }
  

}
