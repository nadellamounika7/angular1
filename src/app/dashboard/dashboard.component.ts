import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatashareService } from '../datashare.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
dataToShow:any;
  constructor(public route:Router,public service:DatashareService) {
    // console.log(this.service.datashare);
    // this.dataToShow=this.service.datashare;
   }

  ngOnInit(): void {
  }
  navigate(){
    this.route.navigate(['about'])
  }

}
