import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent1',
  templateUrl: './parent1.component.html',
  // `<app-child [inputData]="parentMessage"></app-child>`,


  styleUrls: ['./parent1.component.css']
})
export class Parent1Component implements OnInit {
  // inputdata=' ';
  // passworddata=' ';
  messages:any;
  constructor() { }

  ngOnInit(): void {
  }

  receiveMessage($event:any){
    this.messages=$event;
  }

  // parentMessage='data transfer to child'
  // message='second data transfer';

}
