import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
 
  message:string='data transfer from child to parent '
// @Input() 
// inputData:any;
// inputDatta:any;

@Output() event= new EventEmitter;

  constructor() { }

  ngOnInit(): void {
  }
  sendMessage(){
    this.event.emit(this.message)
  }

}
