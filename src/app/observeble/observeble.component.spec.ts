import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservebleComponent } from './observeble.component';

describe('ObservebleComponent', () => {
  let component: ObservebleComponent;
  let fixture: ComponentFixture<ObservebleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObservebleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservebleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
