import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homechild1',
  templateUrl: './homechild1.component.html',
  styleUrls: ['./homechild1.component.css']
})
export class Homechild1Component implements OnInit {

  constructor(public route:Router) { }

  ngOnInit(): void {
  }
  navigate1(){
    this.route.navigate(['home/homechild1/homechild2'])
  }

}
