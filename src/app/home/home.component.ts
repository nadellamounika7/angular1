import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { Router, Routes } from '@angular/router';
import { ObserService } from '../obser.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  twoway=" ";
  twoway1=" ";
  condition=true

  constructor(public route:Router,public data:ObserService) {
    this.data.getMessage().subscribe(res =>{
      console.log("message")
      console.log(res);
      
      
    })
   }

  ngOnInit(): void {
  }
  navigate(){
    this.route.navigate(['dashboard'])
  }
  navigate1(){
    this.route.navigate(['home/homechild1'])
  }
  arr=[{"id":1,"first_name":"Lauree","last_name":"Kneller","email":"lkneller0@quantcast.com","gender":"Female"},
  {"id":2,"first_name":"Samantha","last_name":"Woodfine","email":"swoodfine1@epa.gov","gender":"Female"},
  {"id":3,"first_name":"Rosaline","last_name":"Cammidge","email":"rcammidge2@netlog.com","gender":"Female"},
  {"id":4,"first_name":"Zia","last_name":"Spawell","email":"zspawell3@marketwatch.com","gender":"Female"},
  {"id":5,"first_name":"Mommy","last_name":"Schutze","email":"mschutze4@fotki.com","gender":"Female"},
  {"id":6,"first_name":"Halsy","last_name":"Tarbet","email":"htarbet5@delicious.com","gender":"Male"},
  {"id":7,"first_name":"Giffer","last_name":"Justham","email":"gjustham6@merriam-webster.com","gender":"Genderqueer"},
  {"id":8,"first_name":"Jorry","last_name":"Jojic","email":"jjojic7@engadget.com","gender":"Female"},
  {"id":9,"first_name":"Althea","last_name":"Wogden","email":"awogden8@narod.ru","gender":"Bigender"},
  {"id":10,"first_name":"Annie","last_name":"Courage","email":"acourage9@intel.com","gender":"Female"}]
 
deleteRows(){
  // for(let i=0;i<10;i++)
  {
    this.arr=this.arr.slice(1);
  }
}






    
}
