import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCustomdir]'
})
export class CustomdirDirective {

  constructor(public element:ElementRef) {
    this.element.nativeElement.style.background='grey';    
    this.element.nativeElement.style.color='blue';
    this.element.nativeElement.style.border='5px solid blue';



   }

}
