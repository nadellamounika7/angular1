import { Component } from '@angular/core';
import { DatashareService } from './datashare.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  // title = 'angular1';
  // imgPath="./assets/1.jfif";
  // flower(){
  //   if(this.imgPath=="./assets/1.jfif")
  // this.imgPath="./assets/2.jfif";
  // else{
  //   this.imgPath="./assets/1.jfif";

  // }
  // }
  public date=new Date()

  constructor(private datashare:DatashareService){
    this.datashare.getData().subscribe(res =>{console.log(res)})
  }

  
}
