import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AboutComponent } from './about/about.component';
import { FeaturesComponent } from './features/features.component';
import { UpdatesComponent } from './updates/updates.component';
import { Homechild1Component } from './home/homechild1/homechild1.component';
import { StructuralComponent } from './structural/structural.component';
import { Homechild2Component } from './home/homechild1/homechild2/homechild2.component';
import { CustomdirDirective } from './customdir.directive';
import { InputComponent } from './input/input.component';
import { Parent1Component } from './parent1/parent1.component';
import { ChildComponent } from './parent1/child/child.component';
import { ApiComponent } from './api/api.component';
import { HttpClientModule } from '@angular/common/http';
import { ObservebleComponent } from './observeble/observeble.component';
import { LoginComponent } from './login/login.component';
import { CanDeactivateGuardService } from './can-deactivate-guard.service';
import { FormsComponent } from './forms/forms.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    AboutComponent,
    FeaturesComponent,
    UpdatesComponent,
    Homechild1Component,
    StructuralComponent,
    Homechild2Component,
    CustomdirDirective,
    InputComponent,
    Parent1Component,
    ChildComponent,
    ApiComponent,
    ObservebleComponent,
    LoginComponent,
    FormsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    
  ],
  providers: [CanDeactivateGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
