import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

export interface DeactivationGuarded {
  canDeactivate: () => Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
 }

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuardService implements CanDeactivate<DeactivationGuarded> {

  canDeactivate(component: DeactivationGuarded, 
    route: ActivatedRouteSnapshot, 
    state: RouterStateSnapshot) {
      return component.canDeactivate ? component.canDeactivate() : true;
    }
}
