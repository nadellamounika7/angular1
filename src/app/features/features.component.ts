import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { Event, Router } from '@angular/router';
// import { EventEmitter } from 'stream';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.css']
})
export class FeaturesComponent implements OnInit {
  @Output() event=new EventEmitter();

  constructor(public route:Router) { }

  ngOnInit(): void {
  }
  navigate(){
    this.route.navigate(['updates'])
  }
  // message:string="message pass child to parent";
  sendData(){
    let data:any={name:'mouni',age:50}
    this.event.emit(data)
  }

}
